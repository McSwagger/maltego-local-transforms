﻿using System;
using CommandLine;
using CommandLine.Text;
using System.Collections.Generic;


namespace LocalDomainTransformMaster
{
	public class CommandLineOptions : CommandLine. CommandLineOptionsBase
	{
		[OptionList("c","command",Separator=",", HelpText="Commands to run. You may specify multiple commands separated by a comma")]
		public List<string> Commands { get; set; }

		[HelpOption]
		public string GetUsage()
		{
			


		}
	}
}

