﻿using System;
using MaltegoTransformNet.Core;

namespace LocalDomainTransformMaster
{
	public class CommandDispatcher
	{
		private ActiveDirectoryRepository _activeDirectoryRepo;
		private ActiveDirectoryRepository ActiveDirectoryRepo {
			get{
				if(_activeDirectoryRepo == null){
					_activeDirectoryRepo = new ActiveDirectoryRepository { Maltego = Maltego };
				}

				return _activeDirectoryRepo;
			}
		}

		private NetworkingRepository _networkingRepo;
		private NetworkingRepository NetworkingRepository{
			get{ 
				if(_networkingRepo == null){
					_networkingRepo = new NetworkingRepository ();
				}
				return _networkingRepo;
			}
		}

		private SysInternalsRepository _sysInternalsRepo;
		private SysInternalsRepository SysInternalsRepo{
			get{
				if(_sysInternalsRepo == null){
					_sysInternalsRepo = new SysInternalsRepository ();
				}
				return _sysInternalsRepo;
			}
		}

		private WMIRepository _wmiRepo;
		private WMIRepository WmiRepo{
			get{
				if(_wmiRepo == null){
					_wmiRepo = new WMIRepository ();
				}
				return _wmiRepo;
			}
		}

		private MaltegoResponseGenerator _maltego;
		public MaltegoResponseGenerator Maltego
		{
			get{ 
				if(_maltego == null){
					_maltego = new MaltegoResponseGenerator ();
				}
				return _maltego;
			}
		}

		public void RunCommands(string[] commands, string mainArgument, string maltegoProperties)
		{
			try
			{
				foreach(var command in commands){
					switch(command.ToLowerInvariant()){
					case "internaldomaintocomputers":
						ActiveDirectoryRepo.InternalDomainToComputers (mainArgument);
						break;
					case "internaldomaintoallgroups":
						ActiveDirectoryRepo.InternalDomainToAllGroups (mainArgument);
						break;
					case "internaldomaintosecuritygroups":
						ActiveDirectoryRepo.InternalDomainToSecurityGroups (mainArgument);
						break;
					case "internaldomaintodistributiongroups":
						ActiveDirectoryRepo.InternalDomainToDistributionGroups (mainArgument);
						break;
					case "internaldomaintousers":
						ActiveDirectoryRepo.InternalDomainToUsers (mainArgument);
						break;
					case "grouptosubgroups":
						ActiveDirectoryRepo.GroupToMemberGroups (mainArgument);
						break;
					case "grouptousers":
						ActiveDirectoryRepo.GroupToUsers (mainArgument);
						break;
					case "usertogroups":
						ActiveDirectoryRepo.UserToAllGroups (mainArgument);
						break;
					case "usertodistributiongroups":
						ActiveDirectoryRepo.UserToDistributionGroups (mainArgument);
						break;
					case "usertosecuritygroups":
						ActiveDirectoryRepo.UserToSecurityGroups (mainArgument);
						break;
					case "usertoemailaddresses":
						ActiveDirectoryRepo.UserToEmailAddresses (mainArgument);
						break;
					case "usertophonenumbers":
						ActiveDirectoryRepo.UserToPhoneNumbers (mainArgument);
						break;
					case "internaldomaintoemailaddresses":
						ActiveDirectoryRepo.InternalDomainToEmailAddresses (mainArgument);
						break;
					case "usertoemailread":
						ActiveDirectoryRepo.UserToEmailRead (mainArgument);
						break;
					case "usertoemaildelegates":
						ActiveDirectoryRepo.UserToEmailDelegates (mainArgument);
						break;
					case "computertooperatingsystem":
						ActiveDirectoryRepo.ComputerToOperatingSystem (mainArgument);
						break;
					default:
						Maltego.AddUIMessage (UIMessageType.PartialError, string.Format ("Command: {0} is not a valid command", command));
						break;
					}
				}
			}
			catch(Exception e){
				Maltego.AddUIMessage(UIMessageType.FatalError, string.Format("{0}\r\n{1}\r\n{2}", e.Message, e.Source, e.StackTrace));	
			}

			Console.WriteLine (Maltego.GetMaltegoMessageText());	
		}
	}
}